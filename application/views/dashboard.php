<!--main content start-->
                                  
      <section id="main-content">
          <section class="wrapper">
              <!--state overview start-->
              <div class="row state-overview">
                  <div class="col-lg-4 col-sm-6">
                      <section class="panel">
                          <div class="symbol red">
                              <i class="fa fa-book"></i>
                          </div>
                          <div class="value">
                              <h1>
                                  0
                              </h1>
                              <p>Total Audits this Month</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-4 col-sm-6">
                      <section class="panel">
                          <div class="symbol yellow">
                              <i class="fa fa-plus-square-o"></i>
                          </div>
                          <div class="value">
                              <h1>
                                  0
                              </h1>
                              <p>Pending Audits with New Stores</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-4 col-sm-6">
                      <section class="panel">
                          <div class="symbol terques">
                              <i class="fa fa-users"></i>
                          </div>
                          <div class="value">
                              <h1>
                                <?php echo($total_users); ?>
                              </h1>
                              <p>
                              Active APP users
                              </p>
                          </div>
                      </section>
                  </div>
              </div>
              <!--state overview end-->
          </section>
      </section>
      <!--main content end-->