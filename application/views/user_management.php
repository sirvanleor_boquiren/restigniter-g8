<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						<?php echo $table_header; ?>
					</header>
					<div class="panel-body">
						<div class="table-responsive">
						<div class="pull-right">
							<a href="<?php echo base_url($add_link); ?>" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i>&nbsp;Add User</a><br><br>
						</div>
						<br>
						<?php if(null !== $this->session->flashdata('alert_msg')): ?>
							<div class="form-group">
					          <center>
					            <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
					              <?php echo $this->session->flashdata('alert_msg'); ?>
					            </span>
					          </center>
					        </div>
					        <?php endif; ?>
					      <br>
                          <table class="table table-bordered">
                              <thead>
								<tr>
									<th>Name</th>
									<th>Position</th>
									<th>Mobile</th>
									<th>Email</th>
									<!-- <th>Department</th> Check if needed -->
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
                            <tbody>
							<?php 
							if(count($all_users) > 0)
							{
								foreach ($all_users as $user_id => $user_arr) { ?>
								<tr>
									<td><?php echo $user_arr->fullname; ?></td>
									<td><?php echo $user_arr->pos_desc; ?></td>
									<td><?php echo $user_arr->u_mobile; ?></td>
									<td><?php echo $user_arr->u_email; ?></td>
									<td><?php echo $user_arr->status; ?></td>
									<td>
										<a href="<?php echo base_url("Users/Edit/").$user_arr->u_id; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
										<a href="" class="btn btn-danger btn-xs"><i class="fa fa-lock"></i></a>
									</td>
								</tr>
								<?php
								}
							}
							else
							{ ?>
							<tr>
								<td colspan="6"><center>No records found.<center></td>
							</tr>
							<?php
							} ?>
							</tbody>
							<tfoot>
								<tr>
								  <th>Name</th>
								  <th>Position</th>
								  <th>Mobile</th>
								  <th>Email</th>
								  <th>Status</th>
								  <th>Action</th>
								</tr>
							</tfoot>
                          </table>
                          <div class="text-center">
	                          <ul class="pagination">
	                          	<?php echo $pagination; ?>
	                          </ul>
                          </div>
                        </div>
					</div>
				</section>
			</div>
		</div>
    </section>
</section>
<!--main content end -->