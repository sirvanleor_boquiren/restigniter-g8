<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-2">
			</div>
			<div class="col-sm-8">
				<section class="panel">
					<header class="panel-heading">
						<?php echo $panel_header; ?>
					</header>
					<div class="panel-body">
						<form id="edit_user" name="edit_user" class="form-horizontal tasi-form" method="POST" action="<?php echo $form_action; ?>">
							<?php if(null !== $this->session->flashdata('alert_msg')): ?>
							<div class="form-group">
					          <center>
					            <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
					              <?php echo $this->session->flashdata('alert_msg'); ?>
					            </span>
					          </center>
					        </div>
					        <?php endif; ?>
							<div class="form-group">
								<label class="col-sm-2 control-label">First Name <span class="required">*</span></label>
								<div class="col-sm-10">
									<input class="form-control" type="text" name="u_fname" id="u_fname" value="<?php echo isset($user) ? $user->u_fname : ""; ?>" placeholder="Enter Firstname" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Last Name <span class="required">*</span></label>
								<div class="col-sm-10">
									<input class="form-control" type="text" name="u_lname" id="u_lname" value="<?php echo isset($user) ? $user->u_lname : ""; ?>" placeholder="Enter Lastname" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Middle Initial</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" name="u_minitial" id="u_minitial" value="<?php echo isset($user) ? $user->u_minitial : ""; ?>" placeholder="Enter Initial" style="width:15%">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Email <span class="required">*</span></label>
								<div class="col-sm-10">
									<input class="form-control" type="email" name="u_email" id="u_email" value="<?php echo isset($user) ? $user->u_email : ""; ?>" placeholder="Enter Email" onkeyup="check_email();" required>
									<p class="help-block" id="invalid_email" style="color:red; display:none;">Email is already registered</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Position <span class="required">*</span></label>
								<div class="col-sm-10">
									<select name="pos_id" id="pos_id" class="form-control" required>
										<?php foreach ($positions as $pos_id => $pos_desc) { ?>
										<option value="<?php echo $pos_id; ?>" <?php echo isset($user) && $pos_id == $user->pos_id ? "selected" : ""; ?>><?php echo $pos_desc; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Mobile <span class="required">*</span></label>
								<div class="col-sm-10">
									<input class="form-control" type="number" name="u_mobile" id="u_mobile" value="<?php echo isset($user) ? $user->u_mobile : ""; ?>" placeholder="Enter Mobile Number" required>
								</div>
							</div>
							<?php if(isset($user)): ?>
							<div class="form-group">
								<label class="col-sm-2 control-label">Password</label>
								<div class="col-sm-10">
									<input class="form-control" type="password" name="u_password" id="u_password" value="" placeholder="Enter New Password, Leave Blank if you don't want to change">
								</div>
							</div>
						<?php endif; ?>
							<div class="pull-right">
								<?php if(isset($user)): ?>
								<input type="hidden" name="u_id" id="u_id" value="<?php echo $user->u_id; ?>">
							<?php elseif(isset($is_admin)): ?>
								<input type="hidden" name="u_is_admin" id="u_is_admin" value="<?php echo $is_admin; ?>">
							<?php endif; ?>
								<a href="<?php echo base_url($back_page); ?>" class="btn btn-info btn-shadow">< Back to Table</a>
								<button type="submit" id="submit_edit" class="btn btn-success btn-shadow">Submit</button>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
    </section>
</section>
<!--main content end -->
<?php if(!isset($user)): ?>
<script type="text/javascript">
function check_email()
{
	$.ajax({
	    url:"<?php echo base_url("Ajax/checkEmail"); ?>",
	    type: "POST",
	    data: {"u_email": $('#u_email').val() },
	    success:function(data){ // data is the id of the row
	      if(data == true)
	      {
	      	$('#submit_edit').prop("disabled", "true");
	      	$('#invalid_email').attr("style", "color:red; display:block");
	      }
	      else
	      {
	      	$('#submit_edit').removeAttr("disabled");
	      	$('#invalid_email').attr("style", "color:red; display:none");
	      }
	    },
	    error: function(e){
	      console.log(e);
	    }
	  });
}

</script>
<?php endif; ?>