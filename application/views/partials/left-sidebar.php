<?php 
$sub_menu = array();
$sub_menu["users"] = array("Users/App", "Users/Admin");
$sub_menu["reports"] = array("Reports/Audit", "Reports/Store");

?>
<!--sidebar start-->
<aside>
  <div id="sidebar"  class="nav-collapse ">

    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <li>
        <a <?php echo uri_string() == "Dashboard" ? "class='active'" : ""; ?> href="<?php echo base_url('Dashboard'); ?>">
          <i class="fa fa-dashboard"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" <?php echo in_array(uri_string(), $sub_menu["reports"]) ? "class='active'" : ""; ?>>
          <i class="fa fa-bar-chart-o"></i>
          <span>Reports</span>
        </a>
        <ul class="sub">
          <li>
            <a href="<?php echo base_url('Reports/Audit') ?>" <?php echo uri_string() == "Reports/Audit" ? "class='active'" : ""; ?>>
              <i class="fa fa-book"></i>
              <span>Report Data</span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url('Reports/Store') ?>" <?php echo uri_string() == "Reports/Store" ? "class='active'" : ""; ?>>
              <i class="fa fa-shopping-cart"></i>
              <span>Store Summary</span>
            </a>
          </li>
        </ul>
      </li>

      <li class="sub-menu">
        <a href="javascript:;" <?php echo in_array(uri_string(), $sub_menu["users"]) ? "class='active'" : ""; ?>>
          <i class="fa fa-users"></i>
          <span>Manage Users</span>
        </a>
        <ul class="sub">
          <li>
            <a href="<?php echo base_url('Users/App'); ?>" <?php echo uri_string() == "Users/App" ? "class='active'" : ""; ?>>
              <i class="fa fa-mobile"></i>
              <span>APP Users</span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url('Users/Admin'); ?>" <?php echo uri_string() == "Users/Admin" ? "class='active'" : ""; ?>>
              <i class="fa fa-desktop"></i>
              <span>Admin Users</span>
            </a>
          </li>
        </ul>
      </li>
      <!--multi level menu start-->
      <li class="sub-menu">
          <a href="javascript:;" >
              <i class="fa fa-sitemap"></i>
              <span>Multi level Menu</span>
          </a>
          <ul class="sub">
              <li><a  href="javascript:;">Menu Item 1</a></li>
              <li class="sub-menu">
                  <a  href="boxed_page.html">Menu Item 2</a>
                  <ul class="sub">
                      <li><a  href="javascript:;">Menu Item 2.1</a></li>
                      <li class="sub-menu">
                          <a  href="javascript:;">Menu Item 3</a>
                          <ul class="sub">
                              <li><a  href="javascript:;">Menu Item 3.1</a></li>
                              <li><a  href="javascript:;">Menu Item 3.2</a></li>
                          </ul>
                      </li>
                  </ul>
              </li>
          </ul>
      </li>
      <!--multi level menu end-->

    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->