<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('accounts_model');
    }

    public function index()
	{			
		if(check_login($this->session))
		{
			redirect(base_url("Dashboard"));
		}
		else
		{
			$this->load->view("login");
		}
	}

	public function validate_login()
	{
		$admin_user = $this->accounts_model->verifyLogin($this->input->post(), 1);
		if(is_object($admin_user))
		{
			$this->session->userdata["gmarketing_logged_in"] = GMARKETING_SESSION_KEY;
			$this->session->userdata['id'] = $admin_user->u_id;
			$this->session->userdata['email'] = $admin_user->u_email;
			$this->session->userdata['fname'] = $admin_user->u_fname;
			$this->session->userdata['lname'] = $admin_user->u_lname;
			$this->session->userdata['mobile'] = $admin_user->u_mobile;
			// $this->session->userdata['department'] = $admin_user->u_department;
			// var_dump($this->session->userdata);
			redirect(base_url("Dashboard"));
		}
		else
		{
			$msg_data = array('alert_msg' => 'Incorrect Username/Password', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
			redirect(base_url());
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
}

?>