<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stores extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        # Load all models
        $this->load->model('audit_model');
        $this->load->model('store_model');

        #Load needed libraries not in autoload
        $this->load->library('pagination');

        # Set include array for header and footer
        $this->include = array();
    }

    public function wrapper($body, $data = NULL) 
	{
		if (check_login($this->session)) 
		{
			$this->load->view('partials/header', $this->include);
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer', $this->include);
		}
		else {
			redirect(base_url());
		}
	}

	# DATA Wrapper Functions

	public function Add($sa_id = null)
	{
		# Get All Clients - prepare
		$page_data['clients'] = $this->store_model->getClients();

		# Set Panel Header
		$page_data['panel_header'] = "Add New Store";

		# Set form action
		$page_data['form_action'] = base_url("Stores/addStore");

		# Set back page
		$page_data['back_page'] = "Reports/Audit";

		# Get Store Audit details for store
		if($sa_id != null)
		{
			# Check if $sa_id is valid || avoid hackers/curious users
			if($this->audit_model->checkAudit($sa_id, true))
			{
				# Set Values from audit
				$audit = $this->audit_model->getAudit($sa_id);
				$audit->c_name = $audit->sa_temp_client;				
				$audit->s_name = $audit->sa_temp_store;
				$audit->s_manager = $audit->s_manager_email = "";
				$audit->s_loc_lat = $audit->sa_loc_lat;
				$audit->s_loc_long = $audit->sa_loc_long;
				$audit->sa_id = $sa_id;		
				$page_data['store'] = $audit;

				# Set reject url
				$page_data['reject'] = "Audit/Reject/".$audit->sa_id;	
				$page_data['panel_header'] = "Approve New Store/Client";
			}
			else
			{
				show_404();
			}
		}

		# That's a wrap
		$this->wrapper("store_details.php", $page_data);
	}

	public function addStore()
	{
		$msg_data = array('alert_msg' => 'Please make sure that all required fields are submitted and valid. Please try again.', 'alert_color' => 'red');
		if($this->store_model->addStore($this->input->post()))
		{
			$msg_data = array('alert_msg' => 'Successfully added new store', 'alert_color' => 'green');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('Reports/Audit/'));
	}

}
?>