<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Accounts extends REST_Controller
{

	public function __construct()
	{
		parent::__construct();

		#load models
		$this->load->model("accounts_model");
		$this->load->model("store_model");
	}

	public function login_post()
	{
		$r_msg = array('message' => 'Access Unautorized.');
		$r_status = 401;
		if(check_header_cred())
		{
			$is_error = false;
			# set values to be checked in post
			$check_arr = array("email", "password");
			$r_msg = array("message" => "Bad request");
			$_status = 400;
			foreach ($check_arr as $value) {
				if(null !== $this->post($value))
				{
					if($this->post($value) == "")
					{
						$is_error = true;
						$err_msg = array('status' => '400', 'message' => 'Requried fields are empty.');
						$err_status = 400;
					}
				}
				else
				{
					$is_error = true;
					$err_msg = array('status' => '400', 'message' => 'Required fields passed incomplete.');
					$err_status = 400;
				}
			}
			if($is_error)
			{
				$r_msg = $err_msg;
				$r_status = $err_status;
			}
			else
			{
				$r_msg = array();
				$check_login = $this->accounts_model->verifyLogin($this->post());		
				$r_msg["user"] = $check_login;
				$r_msg["locations"] = $this->store_model->getStores();
				$r_status = 200;
				if(!is_object($check_login))
				{
					if($check_login["error"] == "401")
					{
						$r_msg = array('message' => 'Invalid User Credentials.');
						$r_status = 401;
					}
					else
					{
						$r_msg = array('message' => 'User is not registered');
						$r_status = 401;
					}
				}
			}
		}

		$this->response($r_msg, $r_status);
	}
}


?>