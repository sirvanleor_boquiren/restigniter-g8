<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Audit extends REST_Controller
{

	public function __construct()
	{
		parent::__construct();

		#load models
		$this->load->model("audit_model");
	}

	public function add_post()
	{
		$r_msg = array('message' => 'Access Unautorized.');
		$r_status = 401;
		if(check_header_cred())
		{
			if(null !== $this->post("pos_shortname"))
			{
				if($this->post("pos_shortname") == "AN")
				{
					$is_error = false;
					# Optimize
					# set values to be checked in post
					$check_arr =
					array("sa_dizer", 
						 "sa_visit_date", 
						 "sa_store_cell_num", 
						 "sa_dizer_cell_num", 
						 "sa_loc_long", 
						 "sa_loc_lat", 
						 "sa_stock_value", 
						 "sa_month", 
						 "sa_pullout",
						 "sa_sales",
						 "sa_comments",
						 "sa_analyst");
					$r_msg = array("message" => "Bad request");
					$_status = 400;
					foreach ($check_arr as $value) {
						if(null !== $this->post($value))
						{
							if($this->post($value) == "")
							{
								$is_error = true;
								$err_msg = array('message' => 'Requried fields are empty.');
								$err_status = 400;
							}
						}
						else
						{
							$is_error = true;
							$err_msg = array('message' => 'Required fields passed incomplete.');
							$err_status = 400;
						}
					}

					$allowed_files = array("jpg", "jpeg", "png");

					$check_uploads_arr = 
					array(
						"attachments"
						);
					foreach ($check_uploads_arr as $file_key) {
						if(isset($_FILES[$file_key]))
						{
							foreach ($_FILES[$file_key]['name'] as $image) {
								$image_name = explode(".", $image);
								if(!in_array(strtolower($image_name[1]), $allowed_files))
								{
									$is_error = true;
									$err_msg = array('message' => 'Please upload jpg, jpeg, or png files only.');
									$err_status = 400;
								}
							}	
						}
						else
						{
							$is_error = true;
							$err_msg = array('message' => 'Required fields passed incomplete.');
							$err_status = 400;
						}
						
					}
				}
				elseif($this->post("pos_shortname") == "AG")
				{
					$is_error = false;
				}
				else
				{
					$is_error = true;
					$err_msg = array('message' => 'Position not recognized.');
					$err_status = 400;
				}
			}
			else
			{
				$is_error = true;
				$err_msg = array('message' => 'Required fields passed incomplete.');
				$err_status = 400;

			}

			if($is_error)
			{
				$r_msg = $err_msg;
				$r_status = $err_status;
			}
			else
			{
				$result = $this->audit_model->addStoreAudit($this->post());
				$r_msg["message"] = $result["message"];
				$r_status = $result['status'];
				// $r_status = 400;
			}
			
		}

		$this->response($r_msg, $r_status);	
	}

}


?>