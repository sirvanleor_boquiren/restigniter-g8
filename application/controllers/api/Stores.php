<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Stores extends REST_Controller
{

	public function __construct()
	{
		parent::__construct();

		#load models
		$this->load->model("store_model");
	}

	public function getAll_get()
	{
		$r_msg = array('message' => 'Access Unautorized.');
		$r_status = 401;
		if(check_header_cred())
		{
			$r_msg = $this->store_model->getStores();
			$r_status = "200";
		}
		$this->response($r_msg, $r_status);
	}
}


?>