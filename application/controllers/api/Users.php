<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Users extends REST_Controller {
    
    #GET api/users/
    #GET api/users/user/{id}
    #POST api/users/register
    #PUT api/users/user/{id}
    #DELETE api/users/user/{id}

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Accounts_model");
    }

    public function login_post()
    {
        // $this->load->model("Accounts_model");
        $is_error = false;
        # set values to be checked in post
        $check_arr = array("email", "password", "user_type");
        $r_msg = array("message" => "Bad request");
        $_status = 400;
        foreach ($check_arr as $value) {
            if(null !== $this->post($value))
            {
                if($this->post($value) == "")
                {
                    $is_error = true;
                    $err_msg = array('message' => 'Requried fields are empty.');
                    $err_status = 400;
                }
            }
            else
            {
                $is_error = true;
                $err_msg = array('message' => 'Required fields passed incomplete.');
                $err_status = 400;
            }
        }
        if($is_error)
        {
            $r_msg = $err_msg;
            $r_status = $err_status;
        }
        else
        {
            $check_login = verifyLogin($this->post());
            if($check_login == 401)
            {
                $r_msg = array('message' => 'User is not yet activated.');
                $r_status = $check_login;
            }
            else if($check_login == "false")
            {
                $r_msg = array('message' => 'User is not registered');
                $r_status = 401;
            }
            else
            {
                $r_msg = array('message' => 'Successful user login.');
                $r_status = 200;
            }
        }


        $this->response($r_msg, $r_status);
    }

    public function index_get()
    {
        // Users from a data store e.g. database
        $users = [
            ['id' => 1, 'name' => 'John', 'email' => 'john@example.com', 'fact' => 'Loves coding'],
            ['id' => 2, 'name' => 'Jim', 'email' => 'jim@example.com', 'fact' => 'Developed on CodeIgniter'],
            ['id' => 3, 'name' => 'Jane', 'email' => 'jane@example.com', 'fact' => 'Lives in the USA', ['hobbies' => ['guitar', 'cycling']]],
        ];

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users)
            {
                // Set the response and exit
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    function user_get($id)
    {
        $data = array('returned: '. $id);
        $this->response($data);
    }

    function register_post($id)
    {
        $data = array('returned: ' . $id,
            'name: ' . $this->post('name'),
            'email: ' . $this->post('email'));
        $this->response($data);
    }
 
    function user_put($id)
    {       
        $data = array('returned: '. $id);
        $this->response($data);
    }
 
    function user_delete($id)
    {
        $data = array('returned: '. $id);
        $this->response($data);
    }

    function try_post($id)
    {
        $this->response("NULL", 400);
    }


}
