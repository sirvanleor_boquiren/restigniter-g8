<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        # Load all models
        $this->load->model('accounts_model');
        $this->load->model('audit_model');
        $this->load->model('store_model');

        #Load needed libraries not in autoload
        $this->load->library('pagination');

        # Set include array for header and footer
        $this->include = array();
    }

    public function wrapper($body, $data = NULL) 
	{
		if (check_login($this->session)) 
		{
			$this->load->view('partials/header', $this->include);
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer', $this->include);
		}
		else {
			redirect(base_url());
		}
	}

	# DATA Wrapper Functions

	public function Audit()
	{
		# Ready css/js
		$this->include["responsive_table"] = true;

		# Set Export links
		$page_data["csv_export_link"] = base_url("Reports/auditExportCSV").concat_existing_get();
		$page_data["pdf_export_link"] = base_url("Reports/auditExportPDF").concat_existing_get();

		# Get Pagination
		$pag_conf['base_url'] = base_url()."Reports/Audit";
		$pag_conf['reuse_query_string'] = TRUE;	# maintain get varibles if any
		$pag_conf['total_rows'] = $this->audit_model->getTotalAudit();
		$pag_conf['per_page'] = TABLE_DATA_PER_PAGE;
		$this->pagination->initialize($pag_conf);
		$page_data["pagination"] = $this->pagination->create_links();

		# Get All Audit
		$page_data["all_audit"] = $this->audit_model->getAllAudit();

		# Get All Stores
		$page_data["store_list"] = $this->store_model->getStoresList();

		# Get All Auditors
		$page_data["auditor_list"] = $this->accounts_model->getAllUsers("0");		

		$this->wrapper("audit_management", $page_data);
	}

	public function Store()
	{
		# Ready css/js
		$this->include["responsive_table"] = true;

		# Set Export links
		$page_data["csv_export_link"] = base_url("Reports/storeExportCSV").concat_existing_get();
		$page_data["pdf_export_link"] = base_url("Reports/storeExportPDF").concat_existing_get();

		# Get Pagination
		$pag_conf['base_url'] = base_url()."Reports/Store";
		$pag_conf['reuse_query_string'] = TRUE;	# maintain get varibles if any
		$pag_conf['total_rows'] = $this->store_model->getTotalStores();
		$pag_conf['per_page'] = TABLE_DATA_PER_PAGE;
		$this->pagination->initialize($pag_conf);
		$page_data["pagination"] = $this->pagination->create_links();

		# Get All Summary
		$page_data["all_summary"] = $this->store_model->getSummary();

		$this->wrapper("store_summary_management.php", $page_data);
	}

	# POST Control Functions

	public function assignStore()
	{
		$msg_data = array('alert_msg' => 'Something went wrong. Please try again or contact maintenance.', 'alert_color' => 'red');
		if($this->audit_model->assignStore($this->input->post()))
		{
			$msg_data = array('alert_msg' => 'Store Audit successfully assigned to existing store', 'alert_color' => 'green');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('Reports/Audit/'));
	}

	public function auditExportCSV()
	{
		# Set Column Headers of CSV
		$column_headers = array("Store Code", "Auditor", "Audit Date", "Audit Longitutde", "Audit Latitude", "Grade Per Question", "Average Grade", "Date Submitted");
		$fp = fopen('php://output', 'w');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename=report_data.csv');
		fputcsv($fp, $column_headers);


		# Get All Audit
		$export_data = $this->audit_model->getAllAudit(true);
		foreach ($export_data as $export_arr) {
			if($export_arr->new_store != 1)
			{
				$final_row = array(
					$export_arr->store_code,
					$export_arr->sa_analyst,
					$export_arr->sa_visit_date,
					$export_arr->sa_loc_long,
					$export_arr->sa_loc_lat,
					str_replace("<br>", ", ", $export_arr->grade_per),
					$export_arr->average_grade,
					$export_arr->sa_date_added
					);
				fputcsv($fp, $final_row);
			}
		}
	}

	public function auditExportPDF()
	{
		# Get All Audit
		$export_data = $this->audit_model->getAllAudit(true);
		# Check for filters
		$filter_text = "";
		$filters = array();
		if(isset($_GET['from']) && $_GET['from'] != "")
		{
			$filters[] = "<h4>Audit From - ".date("Y-m-d", strtotime($_GET['from']))."</h4>";
		}
		if(isset($_GET['to']) && $_GET['to'] != "")
		{
			$filters[] = "<h4>Audit to - ".date("Y-m-d", strtotime($_GET['to']))."</h4>";
		}
		if(isset($_GET['store']) && $_GET['store'] != "")
		{
			$filters[] = "<h4>Store - ".$this->store_model->getSpecificStore($_GET['store'])."</h4>";
		}
		if(isset($_GET['auditor']) && $_GET['auditor'] != "")
		{
			$user = $this->accounts_model->getAccount($_GET['auditor']);
			$firstname = $user->u_minitial != "" ? $user->u_fname." ".$user->u_minitial."." : $user->u_fname;
			$user->fullname = $firstname." ".$user->u_lname;
			$filters[] = "<h4>Auditor - ".$user->fullname."</h4>";	
		}

		if(count($filters) > 0)
		{
			$filter_text = "<h3>Filters:</h3><br>".implode("<br>", $filters);
		}

		$tbody = "";
		foreach ($export_data as $export_arr) {
			if($export_arr->new_store != 1)
			{
				$tbody .= "<tr>
						  	<td>".$export_arr->store_code."</td>
						  	<td>".$export_arr->sa_analyst."</td>
						  	<td>".$export_arr->sa_visit_date."</td>
						  	<td>".$export_arr->sa_loc_long."</td>
						  	<td>".$export_arr->sa_loc_lat."</td>
						  	<td>".$export_arr->grade_per."</td>
						  	<td>".$export_arr->average_grade."</td>
						  	<td>".$export_arr->sa_date_added."</td>
						  </tr>";
			}
		}

		$page_data['final_html'] = '
				<h1>Store Summary</h1><br>'.$filter_text.'<br>
				<table border="1" cellspacing="1" cellpadding="5" style="text-align:center;">
					<tr>
						<th style="font-weight:bold">Store Code</th>
						<th style="font-weight:bold">Auditor</th>
						<th style="font-weight:bold">Audit Date</th>
						<th style="font-weight:bold">Audit Longitutde</th>
						<th style="font-weight:bold">Audit Latitude</th>
						<th style="font-weight:bold">Grade Per Question</th>
						<th style="font-weight:bold">Average Grade</th>
						<th style="font-weight:bold">Date Submitted</th>
					</tr>
					'.$tbody.'
				</table>
		';
		// var_dump($page_data['final_html']);
		$this->load->view('export_pdf', $page_data);
	}

	public function storeExportCSV()
	{
		# Set Column Headers of CSV
		$column_headers = array("Last Audit Date", "Store Name", "Store Performance");
		$fp = fopen('php://output', 'w');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename=report_data.csv');
		fputcsv($fp, $column_headers);


		# Get All Store Summary
		$export_data = $this->store_model->getSummary(true);
		foreach ($export_data as $export_arr) {
			$final_row = array(
				$export_arr->sa_visit_date,
				$export_arr->store_name,
				$export_arr->average_grade
				);
			fputcsv($fp, $final_row);
		}
	}

	public function storeExportPDF()
	{
		$export_data = $this->store_model->getSummary(true);
		$filter = "";
		if(isset($_GET['date_range']) && $_GET['date_range'] != "")
		{
			$filter = "<h4>Filtered By: Stores that has not been audited by ".$_GET['date_range']." days</h4>";
		}
		$tbody = "";
		foreach ($export_data as $export_arr) {
			$tbody .= "<tr>
					  	<td>".$export_arr->sa_visit_date."</td>
					  	<td>".$export_arr->store_name."</td>
					  	<td>".$export_arr->average_grade."</td>
					  </tr>";
		}

		$page_data['final_html'] = '
				<h1>Store Summary</h1><br>'.$filter.'<br>
				<table border="1" cellspacing="1" cellpadding="5" style="text-align:center;">
					<tr>
						<th style="font-weight:bold">Last Audit (YYYY-MM-DD)</th>
						<th style="font-weight:bold">Store Name</th>
						<th style="font-weight:bold">Store Performance</th>
					</tr>
					'.$tbody.'
				</table>
		';
		// var_dump($page_data['final_html']);
		$this->load->view('export_pdf', $page_data);
	}



}
?>