<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        # Load all models
        // $this->load->model("positions_model");
        $this->load->model("ajax_model");
        $this->load->model("store_model");
    }


    # User Management Forms
	public function checkEmail()
	{
		echo $this->ajax_model->checkExists($this->input->post(), "users", "u_id", "u_email");
	}

	# Store Details Forms
	public function checkStoreCode()
	{
		echo $this->ajax_model->checkExists($this->input->post(), "stores", "s_id", "s_code");
	}

	public function checkManagerEmail()
	{
		echo $this->ajax_model->checkExists($this->input->post(), "stores", "s_id", "s_manager_email");
	}

	public function checkStoreName()
	{
		echo $this->store_model->checkStoreName($this->input->post());
	}

	public function checkClientName()
	{
		echo $this->ajax_model->checkExists($this->input->post(), "clients", "c_id", "c_name");
	}




}
?>