<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        # Load all models
        $this->load->model('accounts_model');
        $this->load->model('positions_model');

        #Load needed libraries not in autoload
        $this->load->library('pagination');

        # Set include array for header and footer
        $this->include = array();
    }

    public function wrapper($body, $data = NULL) 
	{
		if (check_login($this->session)) 
		{
			$this->load->view('partials/header', $this->include);
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer', $this->include);
		}
		else {
			redirect(base_url());
		}
	}

	# DATA Wrapper Functions

	public function App()
	{
		# Ready css/js
		$this->include["responsive_table"] = true;

		# Get Pagination
		$pag_conf['base_url'] = base_url()."Users/App";
		$pag_conf['total_rows'] = $this->accounts_model->getTotalUsers("0")+$this->accounts_model->getTotalUsers("0", "0"); # active APP users + non active APP users
		$pag_conf['per_page'] = TABLE_DATA_PER_PAGE;
		$this->pagination->initialize($pag_conf);
		$page_data["pagination"] = $this->pagination->create_links();

		# Get users
		$page_data['all_users'] = $this->accounts_model->getAllUsers("0", "");

		# Set Table header
		$page_data["table_header"] = "APP Users";

		# Set Add user link
		$page_data["add_link"] = "Users/Add/App";

		# That's a wrap
		$this->wrapper("user_management", $page_data);
	}

	public function Admin()
	{
		# Ready css/js
		$this->include["responsive_table"] = true;

		# Get Pagination
		$pag_conf['base_url'] = base_url()."Users/App";
		$pag_conf['total_rows'] = $this->accounts_model->getTotalUsers("1")+$this->accounts_model->getTotalUsers("1", "0"); # active APP users + non active APP users
		$pag_conf['per_page'] = TABLE_DATA_PER_PAGE;
		$this->pagination->initialize($pag_conf);
		$page_data["pagination"] = $this->pagination->create_links();

		# Get users
		$page_data['all_users'] = $this->accounts_model->getAllUsers("1", "");

		# Set Table header
		$page_data["table_header"] = "Admin Users";

		# Set Add user link
		$page_data["add_link"] = "Users/Add/Admin";

		# That's a wrap
		$this->wrapper("user_management", $page_data);
	}


	public function Edit($id)
	{
		# Get User Details
		$page_data['user'] = $this->accounts_model->getAccount($id);

		# Get All Positions (Based on user type)
		$page_data['positions'] = $this->positions_model->getAllPositions($page_data['user']->u_is_admin);

		# Set Form action url
		$page_data['form_action'] = base_url("Users/editUser");

		# Set Back Page (Based on user type)
		$page_data['back_page'] = "Users/App";
		if($page_data['user']->u_is_admin == 1)
		{
			$page_data['back_page'] = "Users/Admin";
			$page_data['is_admin'] = 1;
		}

		# Set Panel header
		$page_data['panel_header'] = "Edit User";

		# That's a wrap
		$this->wrapper("user_details.php", $page_data);
	}

	public function Add($type = null)
	{
		$is_admin = 0;
		if($type == "Admin")
		{
			$is_admin = 1;
		}
		elseif($type == "App")
		{
			$is_admin = 0;
		}
		else
		{
			show_404();
		}

		# Get All Positions
		$page_data['positions'] = $this->positions_model->getAllPositions($is_admin);

		# Set Form action url
		$page_data['form_action'] = base_url("Users/addUser/".$type);

		# Set Back Page (Based on user type)
		$page_data['back_page'] = "Users/App";
		if($is_admin == 1)
		{
			$page_data['back_page'] = "Users/Admin";
			$page_data['is_admin'] = 1;
		}

		# Set Panel header
		$page_data['panel_header'] = "Add User";

		# That's a wrap
		$this->wrapper("user_details.php", $page_data);
	}

	# POST Control Functions

	public function editUser()
	{
		$msg_data = array('alert_msg' => 'Something went wrong. Please try again or contact maintenance.', 'alert_color' => 'red');
		if($this->accounts_model->editUser($this->input->post()))
		{
			$msg_data = array('alert_msg' => 'User Successfully Updated', 'alert_color' => 'green');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('Users/Edit/'.$this->input->post("u_id")));
	}

	public function addUser($type = null)
	{
		if($type == "App" || $type == "Admin")
		{
			$is_error = false;
			$required_post = array("u_fname", "u_lname", "u_email", "pos_id", "u_mobile");
			foreach ($required_post as $post_key) {
				if(null !== $this->input->post($post_key))
				{
					if($this->input->post($post_key) == "")
					{
						$is_error = true;
					}
				}
				else
				{
					$is_error = true;
				}
			}
			if($is_error)
			{
				$msg_data = array("alert_msg" => "Required fields not complete. Please complete all required fields.", "alert_color" => "red");
			}
			else
			{
				$msg_data = $this->accounts_model->addUser($this->input->post());
			}
			
			$this->session->set_flashdata($msg_data);
			redirect(base_url('Users/'.$type));
		}
		else
		{
			show_404();
		}
	}

}
?>