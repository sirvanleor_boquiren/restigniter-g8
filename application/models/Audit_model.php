<?php

class Audit_model extends CI_model
{
	public function __construct()
	{
		# ...
	}

	# API

	# Add Store Audit
	public function addStoreAudit($post)
	{
		$response_valid = true;

		/* ----------------- TABLE: store_audit ----------------- */
		$store_audit_values = array();
		$store_audit_columns = 
		array(
			 "c_id",
			 "s_id",
			 "u_id",
			 "sa_dizer", 
			 "sa_visit_date", 
			 "sa_store_cell_num", 
			 "sa_dizer_cell_num", 
			 "sa_loc_long", 
			 "sa_loc_lat", 
			 "sa_stock_value", 
			 "sa_month", 
			 "sa_pullout",
			 "sa_sales",
			 "sa_comments",
			 "sa_analyst",
			 "sa_stock_value",
			 "sa_pullout",
			 "sa_sales",
			 "sa_month",
			 "sa_comments",
			 "sa_temp_store",
			 "sa_temp_client"
			 );
		foreach ($store_audit_columns as $column_name) {
			if(isset($post[$column_name]))
			{
				$store_audit_values[$column_name] = $post[$column_name];
			}
		}

		if(!$this->db->insert('store_audit', $store_audit_values))
		{
			return array("status" => "400", "message" => "Adding Audit failed.");
			$response_valid = false;
		}

		$sa_fk_id = $this->db->insert_id();			# FK for store_audit PK



		/* ----------------- Table: racks_info ----------------- */

		# Add Racks Information for specific Store Audit
		/* 
		* Racks Types
		* 0 - no. of existing racks
		* 1 - wall
		* 2 - island
		* 3 - end cap
		* 4 - media player
		* 5 - HDMI splitter
		*/
		if(isset($post['racks']))
		{
			foreach ($post['racks'] as $racks_arr) {
				$racks_arr['sa_id'] = $sa_fk_id;
				if(!$this->db->insert("racks_info", $racks_arr))
				{
					return array("status" => "400", "message" => "Adding Racks failed.");
					$response_valid = false;
				}
				// $this->addRacksInfo($racks_arr);
			}	
		}

		/* ----------------- Table: attachments ----------------- */
		if(isset($_FILES['attachments']))
		{
			$config = array(
	            'upload_path'   => "./uploads/sa_attachments/",
	            'allowed_types' => 'jpg|png|jpeg',
	            'overwrite'     => 0,                       
	        );
	        $this->load->library('upload', $config);

			$files = $_FILES['attachments'];
			$att_values = array();
			$att_values['sa_id'] = $sa_fk_id;
			$images = array();
			$att_ctr = 0;
			foreach ($files['name'] as $key => $image) {
			    $_FILES['images']['name']= $files['name'][$key];
			    $_FILES['images']['type']= $files['type'][$key];
			    $_FILES['images']['tmp_name']= $files['tmp_name'][$key];
			    $_FILES['images']['error']= $files['error'][$key];
			    $_FILES['images']['size']= $files['size'][$key];
			    $filename = $sa_fk_id."_".$att_ctr.'_'. $image;
			    $images[] = $filename;
			    $config['file_name'] = $filename;
			    $this->upload->initialize($config);
			    if ($this->upload->do_upload('images')) 
			    {
					$att_values['a_file'] = $filename;
					if(!$this->db->insert('attachments', $att_values))
					{
						return array("status" => "400", "message" => "Adding the attached before/after pictures failed.");
						$response_valid = false;
					}
					if(isset($att_values['a_before_id']))
					{
						unset($att_values['a_before_id']);
					}
					if($att_ctr % 2 == 0)
					{
						$att_values['a_before_id'] = $this->db->insert_id();
					}
			    }
			    else
			    {
			        return array("status" => "400", "message" => "Please upload jpg, jpeg, or png files only.");
			        $response_valid = false;
			    }
				$att_ctr++;
			}
		}


		/* ----------------- Table: ratings ----------------- */

		# Add Ratings for specific Store Audit
		/* 
		* Rating Types
		* 0 - Visual
		* 1 - Attendance
		* 2 - Reports
		* 3 - Personality
		* 4 - Overall
		*/
		$ratings_types = array("0"=>"visual", "1"=>"attendance", "2"=>"reports", "3"=>"personality");
		$ratings_values = array();
		$ratings_values['sa_id'] = $sa_fk_id;
		$total_ratings = 0;
		$low_score = array();
		foreach ($ratings_types as $r_type => $r_string) {
			if(isset($post["rate_".$r_string]))
			{
				$ratings_values["r_type"] = $r_type;
				$ratings_values["r_rate"] = $post["rate_".$r_string];
				if(!$this->db->insert("ratings", $ratings_values))
				{
					return array("status" => "400", "message" => "Adding Ratings failed");
					$response_valid = false;
				}
				# Get all ratings for notif later
				$total_ratings += $post["rate_".$r_string];
				if($post["rate_".$r_string] <= 1)
				{
					$low_score[$r_string] = $post["rate_".$r_string];
				}
			}
		}

		if($post["s_id"] != 0)
		{

			$this->load->model('store_model');
			$this->load->library('email');
			if(count($low_score) > 0)
			{
				foreach ($low_score as $rating_text => $rating_grade) {
					$store_name = $this->store_model->getSpecificStore($post['s_id']);
			        $config_mail['charset']='utf-8';
			        $config_mail['newline']="\r\n";
			        $config_mail['wordwrap'] = TRUE;
			        $config_mail['mailtype'] = 'html';
			        $this->email->initialize($config_mail);
					$this->email->from('noreply@g8Marketing.com', 'G8 Marketing Admin');
					$this->email->to("svboquiren@myoptimind.com"); #$post['u_email']
					$this->email->subject('Low Score in a Category');
					$message = "A Store got grade of 1 or below in a category. Details below:<br>";
					$message .= "<br>Store Name: ".$store_name;
					$message .= "<br>Grade: ".$rating_grade;
					$message .= "<br>Category: ".$rating_text;
					$message .= "<br>Auditor: ".$post['sa_analyst'];
					$message .= "<br>Audit Date: ".$post['sa_visit_date'];

					$this->email->message($message);
					if(!$this->email->send())
					{
						return array("status" => "400", "message" => "Sending email error. Please contact maintenance.");
						$response_valid = false;
					}	
				}
			}

			# Check if the overall is less than 2.5
			$ave_grade = ($total_ratings / 4);
			if($ave_grade < 2.5)
			{
				$store_name = $this->store_model->getSpecificStore($post['s_id']);
		        $config_mail['charset']='utf-8';
		        $config_mail['newline']="\r\n";
		        $config_mail['wordwrap'] = TRUE;
		        $config_mail['mailtype'] = 'html';
		        $this->email->initialize($config_mail);
				$this->email->from('noreply@g8Marketing.com', 'G8 Marketing Admin');
				$this->email->to("svboquiren@myoptimind.com"); #$post['u_email']
				$this->email->subject('Overall Grade Notification');
				$message = "A Store got a below 2.5 average grade. Details below:<br>";
				$message .= "<br>Store Name: ".$store_name;
				$message .= "<br>Overall Grade: ".$ave_grade;
				$message .= "<br>Auditor: ".$post['sa_analyst'];
				$message .= "<br>Audit Date: ".$post['sa_visit_date'];

				$this->email->message($message);
				if(!$this->email->send())
				{
					return array("status" => "400", "message" => "Sending email error. Please contact maintenance.");
					$response_valid = false;
				}
			}
		}


		/* ----------------- Table: reports_attachment ----------------- */
		if(isset($_FILES['reports']))
		{
			$config = array(
	            'upload_path'   => "./uploads/sa_reports/",
	            'allowed_types' => 'jpg|png|jpeg',
	            'overwrite'     => 0,                       
	        );
	        $this->load->library('upload', $config);

			$files = $_FILES['reports'];
			$reports_values = array();
			$reports_values['sa_id'] = $sa_fk_id;
			$reports_ctr = 0;
			foreach ($files['name'] as $key => $image) {
			    $_FILES['images']['name']= $files['name'][$key];
			    $_FILES['images']['type']= $files['type'][$key];
			    $_FILES['images']['tmp_name']= $files['tmp_name'][$key];
			    $_FILES['images']['error']= $files['error'][$key];
			    $_FILES['images']['size']= $files['size'][$key];
			    $filename = $sa_fk_id."_".$reports_ctr.'_'. $image;
			    $config['file_name'] = $filename;
			    $this->upload->initialize($config);
			    if ($this->upload->do_upload('images')) 
			    {
					$reports_values['ra_file'] = $filename;
					if(!$this->db->insert('reports_attachment', $reports_values))
					{
						return array("status" => "400", "message" => "Adding additional attachments failed");
						$response_valid = false;
					}
			    }
			    else
			    {
			        return array("status" => "400", "message" => "Please upload jpg, jpeg, or png files only.");
			        $response_valid = false;
			    }
				$reports_ctr++;
			}
		}

		if($response_valid)
		{
			return array("status"=>"200", "message"=>"Adding store audit successful");
		}
	}

	public function addRacksInfo($rack_values)
	{
		$this->db->insert("racks_info", $rack_values);
		return $this->db->affected_rows() > 0;
	}

	public function addRatings($rating_values)
	{
		$this->db->insert("ratings", $rating_values);
		return $this->db->affected_rows() > 0;
	}

	# CMS

	# Get Total Audit
	public function getTotalAudit()
	{
		$this->db->select("sa_id");
		$this->db->from("store_audit");

		# Check for filters
		$where = "";
		$get_var = array(
			"from" => "sa_visit_date >= ",
			"to" => "sa_visit_date <= ",
			"store" => "s_id = ",
			"auditor" => "u_id = "
			);

		$where_arr = array();
		foreach ($get_var as $get_key => $get_value) {
			if(isset($_GET[$get_key]) && $_GET[$get_key] != "")
			{
				if($get_key == "from" || $get_key == "to")
				{
					$_GET[$get_key] = date("Y-m-d H:i:s", strtotime($_GET[$get_key]));
				}
				$where_arr[] = $get_value."'".$_GET[$get_key]."'";
			}
		}

		$where = implode(" AND ", $where_arr);
		if($where != "")
		{
			$this->db->where($where);
		}

		return $this->db->get()->num_rows();
	}

	# Get specific audit
	public function getAudit($sa_id)
	{
		$this->db->from("store_audit");
		$this->db->where("sa_id = ".$sa_id);
		$db_audit = $this->db->get();
		if($db_audit->num_rows() > 0)
		{
			return $db_audit->result()[0];
		}
		else
		{
			return null;
		}
	}

	public function checkAudit($sa_id, $check_approve = false)
	{
		$this->db->select("sa_id, s_id");
		$this->db->from("store_audit");
		$this->db->where("sa_id = ".$sa_id);
		$db_audit = $this->db->get();
		if($db_audit->num_rows() > 0)
		{
			if($check_approve)
			{
				if($db_audit->result()[0]->s_id != "0")
				{
					# Audit already has a client & store; doesn't need approving
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			# Audit does not exists
			return false;
		}
	}

	# Assign Store Audit to existing Store
	public function assignStore($post)
	{
		$this->db->where("sa_id", $post['sa_id']);
		unset($post['sa_id']); # Prevent overwrite/conflict
		return $this->db->update("store_audit", $post);

	}

	# Get All Audit
	public function getAllAudit($is_reports = false) # $is_reports is when called for exporting data, which outputs no limit
	{		
		$this->db->from("store_audit");
		$this->db->order_by("sa_visit_date", "desc");

		# Check for filters
		$where = "";
		$get_var = array(
			"from" => "sa_visit_date >= ",
			"to" => "sa_visit_date <= ",
			"store" => "s_id = ",
			"auditor" => "u_id = "
			);

		$where_arr = array();
		foreach ($get_var as $get_key => $get_value) {
			if(isset($_GET[$get_key]) && $_GET[$get_key] != "")
			{
				if($get_key == "from" || $get_key == "to")
				{
					$_GET[$get_key] = date("Y-m-d H:i:s", strtotime($_GET[$get_key]));
				}
				$where_arr[] = $get_value."'".$_GET[$get_key]."'";
			}
		}

		$where = implode(" AND ", $where_arr);
		if($where != "")
		{
			$this->db->where($where);
		}
		
		if(!$is_reports)
		{
			# Pagination set limit
			$limit = 0;
			if($this->uri->segment(3) !== FALSE)
			{
				$limit = $this->uri->segment(3);
			}
			$this->db->limit(TABLE_DATA_PER_PAGE, $limit); # number_per_page, start row
		}

		$db_results = $this->db->get();

		if($db_results->num_rows() > 0)
		{
			$db_data = array();
			foreach ($db_results->result() as $audit) {
				# Set defaults assuming that the user inputted new client and store
				$audit->new_store = "1";
				$audit->client_name = $audit->sa_temp_client;
				$audit->store_name = $audit->sa_temp_store;
				$audit->store_code = "Store for approval <br><br>";
				$audit->s_loc_lat = 0;
				$audit->s_loc_long = 0;
				if($audit->c_id == 0)
				{
					$audit->store_code .= "Client: <strong>".$audit->client_name."</strong><br>";
				}
				$audit->store_code .= "Store: <strong>".$audit->store_name."</strong><br>";

				# Check if audit used existing client and store
				if($audit->s_id != 0)
				{
					$audit->new_store = "0";
					$this->db->select("s_id, s_name, stores.c_id, c_name, stores.s_code, stores.s_loc_lat, stores.s_loc_long");
					$this->db->from("stores");
					$this->db->join("clients", "stores.c_id = clients.c_id");
					$this->db->where("s_id = ".$audit->s_id);
					$db_store = $this->db->get();
					if($db_store->num_rows() > 0)
					{
						foreach ($db_store->result() as $store) {
							$audit->client_name = $store->c_name;
							$audit->store_name = $store->s_name;
							$audit->store_code = $store->s_code;
							$audit->s_loc_lat = $store->s_loc_lat;
							$audit->s_loc_long = $store->s_loc_long;
						}
					}
				}

				# Get Ratings
				$this->db->from("ratings");
				$this->db->where("sa_id = ".$audit->sa_id);
				$this->db->order_by("r_type", "asc");
				$db_ratings = $this->db->get();
				$audit->grade_per = "N/A";
				$audit->store_rating = "N/A";
				if($db_ratings->num_rows() > 0)
				{
					# Set Defaults blank
					$grade_per_arr = array();
					$grade_total = 0;
					$max_grade = 0;
					$audit->grade_per = "";
					$audit->store_rating = "";
					
					# Get Grade per Question
					foreach ($db_ratings->result() as $rating) {
						$grade_per_arr[] = get_rating_desc($rating->r_type)." : ".$rating->r_rate;

						# For getting Stars
						$grade_total += $rating->r_rate;
						$max_grade += 5;

					}
					
					# Final Grade per Question
					$audit->grade_per = implode("<br>", $grade_per_arr);

					# Final Average Grade
					$audit->average_grade = $grade_total / 4;

					# Get Stars (percentage of the average)
					$percent =  ($grade_total / $max_grade) * 100;

					# Make loop dynamic depending on stars
					$no_of_stars = 5;
					$percent_decrement = 100 / $no_of_stars;

					for ($i=$no_of_stars; $i>0; $i--) {
						$star = "star ";
						
						# IF star is half
						if($percent < $percent_decrement && $percent > 0)
						{
							$star = "star-half ";
						}
						
						# IF star is empty
						if($percent <= 0)
						{
							$star = "star-disabled ";
						}

						# Append return data for store rating		
						$audit->store_rating .= "<span class='".$star."'></span>";

						# Decrement percent, 100 (percent) / $i (amount of stars)
						$percent -= $percent_decrement;
					}
					
				}
				
				# Save to array final row data
				$db_data[] = $audit;
			}
			return $db_data;
		}
		else
		{
			return null;
		}

	}


}

?>