<?php

function check_login($session_var)
{
	if(isset($session_var->userdata['gmarketing_logged_in']) 
		&& $session_var->userdata['gmarketing_logged_in'] == GMARKETING_SESSION_KEY)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function check_header_cred()
{
	# check access
	$access = false;
	foreach(getallheaders() as $key => $value){
		if(strtoupper($key) == "G8-HEADER-KEY")
		{
			if($value == G8_HEADER_KEY)
			{
				$access = true;
			}
		}
	}
	return $access;
}

function get_rating_desc($rating_type)
{
	$rating_desc = array(
		"0" => "Visual",
		"1" => "Attendance",
		"2" => "Reports",
		"3" => "Personality",
		"4" => "Overall"
		);

	return $rating_desc[$rating_type];
}

function concat_existing_get()
{
	$pagination_a_href = "";
	$string_get = "";
	if(isset($_GET))
	{
	  $existing_get = array();
	  foreach ($_GET as $get_key => $get_value) {
	    if($get_key != $settings['get_var'])
	    {
	      $existing_get[] = $get_key."=".$get_value;
	    }
	    $string_get = implode("&", $existing_get);
	  }
	}
	if($string_get != "")
	{
		$pagination_a_href = "/?".$string_get;
	}
	
	return $pagination_a_href;


}
	
?>